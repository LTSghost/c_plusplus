# 註解(comments)
// **單行** 、 /* **概括的部分都註解**  */ 
``` c++
int main(){
    reutrn 0; //程式主體的部分
} 
------------------------------
/*
int main(){
    return 0;
}*/
```

# 前處理指令(preprocessor directives)
以符號 **#** 開頭的指令會先進行前處理程序 **必須佔據獨立一行**
``` c++
#include <iostream>     // iostream 讓我們能使用 cout,endl,<<,>> etc.
using std:: cout;       // using宣告 cout是名稱空間std中的標準元件，即將在程式中使用 
using std:: endl; 

using namespace std;    //  可使用C++標準程式庫的任何識別符號  就不需要一個一個using std
```

# 程式主體(main)
不管 **main()** 是不是程式中第一個出現的函數，程式都從 **main()** 開始執行。
main左邊的 **int** 表示main()的執行結果會回傳一個整數，**int = integer(縮寫)**
``` c++
#include <iostream>
using namespace std;

//--程式主體部分-------------------
int main(){
    cout << "Hello World!" << endl;     //主要敘述
    return 0;				// return 回傳 0
}
```

# 敘述(statement)
每一個敘述都是以 **;** 分號做結尾，在輸出敘述中 **"Hello, World!"**　稱為字串
，使用 **<<** 運算子，將字串送到 **cout** 輸出資料流物件
，再將 **endl** 格式操縱子來設定輸出格式，強迫暫存器內的資料立刻輸出。
當 **endl** 和 **cout** 一起使用時，它的效果相當於將存在輸出資料流內的結果立刻顯示出來
，並且換行。
``` c++
cout << "Hello World!" << endl;
cout << "World : Hello hello~" << endl;
cout << "Hello : World hello~" << endl;
```

# 變數的宣告
所有程式內的 **變數(variables)** 都要經過 **宣告(declare)**
變數經過宣告後，就會依指定的 **資料型態(data type)** 配置記憶體
例如以下 **x** 就是設定 **int** 而 **int占用 4 bytes，1 byte = 8 bits**，因此這個 **變數**
就佔據了 **32bits**

[相關資料類型範圍](https://docs.microsoft.com/zh-tw/cpp/cpp/data-type-ranges?view=msvc-160)

``` c++
int x = 0,y,z;  //設定變數 x,y,z 資料型態為 int

// or

int x = 0;  //可同時給于初始值 0
int y;
int z;
```

# 簡單求和程式
使用者輸入兩個整數，並把計算結果輸出
``` c++
#include <iostream>
using namespace std;

int main() {
	int a, b, sum;
	cout << "輸入第一個整數a: \n";		//字元'\n'為逃離字元跳到下一行
	cin >> a;				//  cin 為輸入資料流物件 
						//  >> 為資料流擷取運算子 把來自鍵盤的輸入讀進執行中的程式
	cout << "輸入第二個整數b: " << endl;
	cin >> b;
	sum = a + b;
	cout << endl;
	cout << "a + b = " << sum << endl;
	return 0;
}
```
