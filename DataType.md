# C++ 基本資料型態
## 基本資料型態
``` c++
//整數
int, short, long
//浮點數
float, double, long double
//字元
char
//邏輯值
bool()
```
## 衍生資料型態
位址相關 

**指標, 參照**

結構型
``` c++
string
enum
array
struct
union
class
```

# 整數(integer values)
所有不具小數點的數值 ex :
``` c++
6 -3287 0 +248517 +0 005 4L

48U     // unsigned int
75UL    // unsigned long
372L    // long
012     // 8進位的12 = 10
0x12    // 16進位的12 = 18
```
# 浮點數(floating point numbers)
必須帶有小數點的值 ex :
``` c++
36.0 -7.845 +3.92L 0.00 +4.0  3.7f  3.7F

2413.665        //10進制
2.413665e+3     //科學表示法 E/e = exponent
2.413665e3
2.413665E+3
2.413665E3

-0.0000624      //10進制
-6.24e-5        //exponential notation
-6.24E+5
```
# 變數(variables)、常數(constants)
所有資料必須放入 **主記憶體** 內才能運作
``` c++
//變數  允許變更其內容
int a;      // 資料型態 int 32bits 名稱 a
short       // 資料型態 short int 16bits
long        // 資料型態 long int 32bits
long long   // 資料型態 long long int 64bits

float b;    // 資料型態 float 32bits 名稱 b  小數以下有效位元7
double      // 資料型態 double 64bits       小數以下有效位元16
long double // 資料型態 double 64bits       小數以下有效位元16

//測試資料型態範圍
#include <iostream>
using namespace std;

int main() {
	long double a;
	cout << sizeof(a) << endl;		//用sizeof()  會顯示 8  表示 8 bytes
	return 0;
}

//常數  不允許變更其值
const a = 5;
a = 6;		//錯誤,常數不能給于新值
```
# 算術運算
用於算術運算符號，稱為 **算術運算子(arithmetic operators)**
``` c++
int a = 6, b = 4, c;
int a1 = 6.0;
a + b = 10			
a - b = 2
a * b = 24
a / b = 1			// 整數除法不包括小數點
a1 / b = 1.5			// 浮點數 +-*/ 整數  會取較精準的表示方式 所以會是 1.5 
a % b = 2
6 + a * b = 30			// 遵循先乘除後加減


c = a++;			// c 會是 6  a++意思是把a放入c 再執行a = a+1
c = ++b;			// c 會是 5  ++b意思執行b = b+1 再把a放入b 
c = a--;			// c 會是 7  此時a因為上面++過 所以會是7  然後再執行 a = a-1
c = --b;			// c 會是 4  此時b因為上面++過 所以會是5 但是先執行 b = b-1 所以會是4
c = ++a * 5;			//相當於 a++; c = a*5;

//可簡化
c = c+a;
c += a;
c = c*a;
c *= a;   //...以此類推
```
# 常用數學函式
``` c++
#include <iostream>
using namespace std;

int main() {
	int a = -16 ,c = 4;
	float b = -16.16, d = 1.999, f = 8;
	double e = 10;

#define PI 3.14159265

	cout << "abs(a) = " << abs(a) << endl;		//取int絕對值 = 16 
	cout << "fabs(b) = " << fabs(b) << endl;	//取float絕對值 = 16 
	cout << "exp(a) = " << exp(a) << endl;		//自然數的次方 e^0 = 1
	cout << "pow(a, c) = " << pow(a,c) << endl;	// a的c次方 = 65536
	cout << "sqrt(c) = " << sqrt(c) << endl;	// c的開根號 = 2
	cout << "log(a) = " << log(exp(a)) << endl;	// log自然數為底的e的a 所以是 a 
	cout << "log10(e) = " << log10(e) << endl;	// log10為底的10對數 = 1
	cout << "ceil(d) = " << ceil(d) << endl;	// 1.999取天花板 >= d的最小整數
	cout << "floor(d) = " << floor(d) << endl;	// 1.999取地板 <= d的最大整數


	cout << "sin(30) = " << sin(30 * PI/180) << endl;	//函數輸入的是弧度，要先 *PI/180 角度轉成弧度才能運算
	cout << "cos(30) = " << cos(30 * PI / 180) << endl;
	cout << "tan(30) = " << tan(30 * PI / 180) << endl;

	return 0;
}
```
# 邏輯值及其運算
邏輯值又稱 **布林資料(Boolean data)**，只有 **True** 和 **False**
，在電腦內部分別儲成整數 **1** 和 **0** 任何不是 **0** 的布林資料判斷都視為 **True**

### 關係運算子(Relational Operations)
``` c++
   <	小於
   >	大於
   <=	小於或等於
   >=	大於或等於
   ==	相等
   !=	不相等
```
### 邏輯運算子(Logical Operations)
``` c++
   &&	AND	(a > b) && (c > d)	// a && b 如果 a 為0 則輸出為0
					//	  如果 a 為1 則輸出為 b
   ||	OR	(x > 8.2) && (y < 0.0)  // a || b 如果 a 為1 則輸出為1
					//	  如果 a 為0 則輸出為 b
   !	NOT	!( x < 0.0)		// a = 1, !a = 0
```
### De Morgan
``` c++
   !(x == a && y != b && z != c)
/*
   not (a and b) 等於 (not A) or (not B)  定理
   not (a or b) 等於 (not A) and (not B)  定理	
*/
   上式De Morgan轉換 
=> (x != a || y == b || z == c)
```

# 字元與字串
C++ 將文字資料以 **字元(character)** 和 **字串(string)** 表示

### 字元資料包括:
1. 英文 A ~ Z, a ~ z
2. 數字 0 ~ 9
3. 特別符號, + $ - @ ! etc.

共128個符號，所有字元都用單引號包起來:
``` c++
'a'  'A'  '$'  '+'  '5'
```
字元以 **ASCII碼** 儲存。每個字元占 **一個byte**
 
[**美國資訊交換標準碼(American Standard Code for Information Interchange)**](https://zh.wikipedia.org/wiki/ASCII)
``` c++
	a 存成 0110 0001
	b 存成 0110 0010
	A 存成 0100 0001
	Q 存成 0101 0001		// Q為A+16 所以+16就可以找出Q的值

	字元的資料型態為 char 
	
	char ch = 'f' ;
	cout << ch << endl;  // 輸出為 f 字母 不是'f'
```
### 字串
**字串(string)** 是一序列用雙引號「"」包起來的字元。
在儲存的時候，所有字串結尾自動加上 **'\0'** 表示字串的結束。
而且中文字都是以 **兩個bytes儲存**

例如:
``` c++
"c+b="			// 使用 5 bytes
"你好"			// 使用 5 bytes
"The answer is: "	// 使用 16 bytes
"\nCorrect"		// 使用 9 bytes  '\n' 逃離序列的字元都是一個byte
```
測試字串的大小位元組 使用 **sizeof()** 語法:
``` c++
cout << sizeof("哈囉你好嗎") << endl; //輸出結果會為 11 bytes
------------------------------
// 字元也可以進行運算
cout << 'a' + 0 << endl; //值為97
cout << '0' + 0 << endl; //值為48
cout << '\0' + 1 << endl; //值為1
```

# 位元處理運算
C++能夠處理最低的資料型態: **位元**

### 位元運算子(bitwise operator)
``` c++
   ~	補數(complement)  (~'a') = 1001 1110	//10進位是-98 1'要轉成2'
   <<	左移	      ('a' << 2) = 1 1000 0100	// 256+128+4=388
   >>	右移	      ('a' >> 2) = 0001 1000	// 16+8=24
   &	且(AND)	     ('a' & '\0') = 0000 0000	// 0
   ^	XOR	       ('a' ^ 127) = 0001 1110	// 16+14=30
   |	或(OR)	       ('a' | 127) = 0111 1111	// 127
```

