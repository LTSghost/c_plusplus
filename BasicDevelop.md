# 基本程式開發步驟

## Central Processing Unit(CPU) only execute **machine code*  so every High-level programming language needed **compile* to **machine code*

### C++ program development incldue 6 steps :
#### 1. 編輯(edit)
藉由 **編輯器(editor) : Visual Studio** 撰寫完成的程式檔案 **原始程式碼(source code)**  C++檔案格式 **.cpp** 和 **.h**
#### 2. 前處理(preprocess)
編譯前先依照程式內的 **前處理指令(preprocessor directives)** 進行編譯前的特定處理 好比 : **include< iostream >** 先引入相關程式庫
#### 3. 編譯(compile)
**編譯器(compiler)** 功能將前處理器產生的 **中間檔**(又稱 **轉譯單元,translation unit**) 轉譯成 **二進位的機器碼** 稱為 **目的檔(object file)** 
並同時進行語法檢查和程式碼最佳化。為了簡化編譯器的設計工作，某些系統的編譯器所產生的檔案是我們能夠閱讀的 
**組合語言程式碼(assembly code)** ，其後再自動呼叫 **組合器(assembler)** 產生 **目的檔(object file)** 。
 **目的檔** 中各個指令和資料的位址只是 **相對位址(relative address)**，還不是 **絕對位址(absolute address)**。
#### 4. 聯結(link)
最後編譯階段稱為 **聯結(linking)**。程式多少都會引用一些已經編譯成機器碼的程式片段(這些程式片段通常集合成 **程式庫** )。
**聯結器(linker)** 的工作是將 **目的檔** 與在程式中引用的程式庫機器碼聯結成完整的 **執行檔(executable file)**。
此外，聯結器還進行了將 **相對位址** 轉換為 **絕對位址** 的工作。
#### 5. 裝載(load)
在程式執行前，需要 **裝載器(loader)** 的協助把 **可執行碼(executable code)**，放置於主記憶體內。
在某些情況下，這個裝載的動作包括 **動態聯結檔(DLL，Dynamically Linked Libraries，具有附檔名.dll)的加入**。
> DLL是軟體工程技術,把執行檔切成小檔案,可以選擇性的在執行時聯結特定的檔案。在有update跟restore時,也只需要取代部分DLL檔即可
#### 6. 執行(execute)
執行軟體


